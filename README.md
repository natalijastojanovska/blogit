# BlogIt! 💻
Welcome to BlogIt! BlogIt is a full stack blog application by which you can share your ideas with the world! Made with Node.js and Express.js, Blogit uses EJS template engine to dynamically create a webpage for your blog. CRUD operations on your blogs are done with MongoDB. You can compose as many blogs as you like and people can subscribe to your newsletter. ✨


# How to use this? 🤔
If your want to try this app on your local machine, follow these steps: 
1. Clone this repository in your local environment by the following command:
```git clone https://gitlab.com/natalijastojanovska/blogit.git```

2. Use NPM (Node Package Manager) to install dependencies for this project. <br>
```npm install```

3. Use MongoDB service to set up your database, than use Node.js to start the application: <br>
```node app.js```

4. Go to `localhost:3000` on your browser, and the homepage of BlogIt will appear. You can read blogs from here and click `Read more` to, well, read more!

5. Now you can go to the Compose tab to publish your own blog. Your blog will be visible on the homepage, where you will also find a link to the dynamically created page and URL for your blog.

6. For updating the blog, you can go to the Update tab and search for the blog you want to update. After that, you can add new title, content or image to the blog.

7. If you open a Subscrtibe tab, you will see a form, where people can subscribe to your newsletter.

# How it works? 🛠
This web app uses Node.js for backend and Express.js for serving static files, using middlewares and generating URL's for blogs with routes. The main part of the app is based on EJS, which is a template engine. As soon as you publish the blog, it gets saved on the MongoDB database with Mongoose and a new `div` is generated for your blogs. A new URL is also generated, as `/posts/your-title` by which you can access your blog. The compose blog page can be found at `/compose/`. All the blogs are saved on a MongoDB database. You can also update your blogs if you want to change the title, content or the image associated with any of the blog. All these CRUD operations are carried out on MongoDB with Mongoose. 

# Inspiration 💡
This project was initially started as a course-along project which I made while learning Node.js and Express.js
